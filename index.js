const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017/users';

MongoClient.connect(url, (err, db) => {
    if (err) {
        console.log('Произошла ошибка: ', err);
    }
    else {
        let collection = db.collection('users');

        let arNames = ['Маша', 'Оля'];
        let arUsers = [
            {
                name: 'Аня'
            },
            {
                name: 'Маша'
            },
            {
                name: 'Катя'
            },
            {
                name: 'Оля'
            }
        ];
        collection.insert(arUsers, (err, res) => {
            if (err) {
                console.log('Произошла ошибка: ', err);
            }
            else {
                collection.find().toArray((err, res) => {
                    if (err) {
                        console.log('Произошла ошибка: ', err);
                    }
                    else {
                        if (res.length) {
                            console.log(res);
                            collection.updateMany(
                                {name: {$in: arNames}},
                                {$set: {'name': 'Петя'}},
                                (err) => {
                                    if (err) {
                                        console.log('Произошла ошибка: ', err);
                                    }
                                    else {
                                        collection.find().toArray((err, res) => {
                                            if (err) {
                                                console.log('Произошла ошибка: ', err);
                                            }
                                            else {
                                                if (res.length) {
                                                    console.log(res);
                                                }
                                                else {
                                                    console.log('Коллекция пуста');
                                                }
                                                collection.remove({name: 'Петя'},{},(err, res) => {
                                                    if (err) {
                                                        console.log('Произошла ошибка: ', err);
                                                    }
                                                    else {
                                                        console.log(`Удалено ${res.result.n} документов!`)
                                                    }
                                                    db.close();
                                                });
                                            }
                                        });
                                    }
                                }
                            );
                        }
                        else {
                            console.log('Коллекция пуста');
                        }
                    }
                });
            }
        });
    }
});